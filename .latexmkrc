@default_files = (
    'dagvandewetenschap.tex',
);
$out_dir = 'out';
$pdf_mode = 1;
system('gs -dNoOutputFonts \
           -sDEVICE=pdfwrite \
	   -sDEVICE=pdfwrite \
           -dColorConversionStrategy=/LeaveColorUnchanged \
           -dDownsampleMonoImages=false \
           -dDownsampleGrayImages=false \
           -dDownsampleColorImages=false \
           -dAutoFilterColorImages=false \
           -dAutoFilterGrayImages=false \
           -dColorImageFilter=/FlateEncode \
           -dGrayImageFilter=/FlateEncode \
	   -o out/dagvandewetenschap_nofonts.pdf \
	   out/dagvandewetenschap.pdf')
